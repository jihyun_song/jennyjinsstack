
from sqlalchemy import Boolean, Column, DateTime
from sqlalchemy import MetaData, Integer, String, Table

from cinder.openstack.common import log as logging

LOG = logging.getLogger(__name__)


def upgrade(migrate_engine):
    meta = MetaData()
    meta.bind = migrate_engine

    # New table
    gateways = Table(
        'gateways', meta,
        Column('created_at', DateTime(timezone=False)),
        Column('updated_at', DateTime(timezone=False)),
        Column('deleted_at', DateTime(timezone=False)),
        Column('deleted', Boolean(create_constraint=True, name=None)),
        Column('id', String(36), primary_key=True, nullable=False),
        Column('volume_id', String(36), nullable=False),
        Column('user_id', String(length=255)),
        Column('project_id', String(length=255)),
        Column('host', String(length=255)),
        Column('availability_zone', String(length=255)),
        Column('display_name', String(length=255)),
        Column('display_description', String(length=255)),
        Column('container', String(length=255)),
        Column('status', String(length=255)),
        Column('fail_reason', String(length=255)),
        Column('service_metadata', String(length=255)),
        Column('service', String(length=255)),
        Column('size', Integer()),
        Column('object_count', Integer()),
        mysql_engine='InnoDB'
    )

    try:
        gateways.create()
    except Exception:
        LOG.error(_("Table |%s| not created!"), repr(gateways))
        raise


def downgrade(migrate_engine):
    meta = MetaData()
    meta.bind = migrate_engine

    gateways = Table('gateways', meta, autoload=True)
    try:
        gateways.drop()
    except Exception:
        LOG.error(_("gateways table not dropped"))
        raise
