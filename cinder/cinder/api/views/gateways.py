from cinder.api import common
from cinder.openstack.common import log as logging


LOG = logging.getLogger(__name__)


class ViewBuilder(common.ViewBuilder):
    """Model gateway API responses as a python dictionary."""

    _collection_name = "gateways"

    def __init__(self):
        """Initialize view builder."""
        super(ViewBuilder, self).__init__()

    def summary_list(self, request, gateways):
        """Show a list of gateways without many details."""
        return self._list_view(self.summary, request, gateways)

    def detail_list(self, request, gateways):
        """Detailed view of a list of gateways ."""
        return self._list_view(self.detail, request, gateways)

    def summary(self, request, gateway):
        """Generic, non-detailed view of a gateway."""
        return {
            'gateway': {
                'id': gateway['id'],
                'name': gateway['display_name'],
                'links': self._get_links(request,
                                         gateway['id']),
            },
        }

    def restore_summary(self, request, restore):
        """Generic, non-detailed view of a restore."""
        return {
            'restore': {
                'gateway_id': restore['gateway_id'],
                'volume_id': restore['volume_id'],
            },
        }

    def detail(self, request, gateway):
        """Detailed view of a single gatewa."""
        return {
            'gateway': {
                'id': gateway.get('id'),
                'status': gateway.get('status'),
                'size': gateway.get('size'),
                'object_count': gateway.get('object_count'),
                'availability_zone': gateway.get('availability_zone'),
                'container': gateway.get('container'),
                'created_at': gateway.get('created_at'),
                'name': gateway.get('display_name'),
                'description': gateway.get('display_description'),
                'fail_reason': gateway.get('fail_reason'),
                'volume_id': gateway.get('volume_id'),
                'links': self._get_links(request, gateway['id'])
            }
        }

    def _list_view(self, func, request, gateways):
        """Provide a view for a list of gateways."""
        gateways_list = [func(request, gateway)['gateway'] for gateway in gateways]
        gateways_links = self._get_collection_links(request,
                                                   gateways,
                                                   self._collection_name)
        gateways_dict = dict(gateways=gateways_list)

        if gateways_links:
            gateways_dict['gateways_links'] = gateways_links

        return gateways_dict
