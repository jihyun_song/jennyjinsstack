# Copyright (C) 2012 Hewlett-Packard Development Company, L.P.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

"""The gateways api."""


import webob
from webob import exc
from xml.dom import minidom

from cinder.api import common
from cinder.api import extensions
from cinder.api.openstack import wsgi
from cinder.api.views import gateways as gateway_views
from cinder.api import xmlutil
from cinder import gateway as gatewayAPI
from cinder import exception
from cinder.openstack.common import log as logging


LOG = logging.getLogger(__name__)


def make_gateway(elem):
    elem.set('id')
    elem.set('status')
    elem.set('size')
    elem.set('container')
    elem.set('volume_id')
    elem.set('object_count')
    elem.set('availability_zone')
    elem.set('created_at')
    elem.set('name')
    elem.set('description')
    elem.set('fail_reason')


def make_gateway_restore(elem):
    elem.set('gateway_id')
    elem.set('volume_id')


class GatewayTemplate(xmlutil.TemplateBuilder):
    def construct(self):
        root = xmlutil.TemplateElement('gateway', selector='gateway')
        make_gateway(root)
        alias = Gateways.alias
        namespace = Gateways.namespace
        return xmlutil.MasterTemplate(root, 1, nsmap={alias: namespace})


class GatewaysTemplate(xmlutil.TemplateBuilder):
    def construct(self):
        root = xmlutil.TemplateElement('gateways')
        elem = xmlutil.SubTemplateElement(root, 'gateway', selector='gateways')
        make_gateway(elem)
        alias = Gateways.alias
        namespace = Gateways.namespace
        return xmlutil.MasterTemplate(root, 1, nsmap={alias: namespace})


class GatewayRestoreTemplate(xmlutil.TemplateBuilder):
    def construct(self):
        root = xmlutil.TemplateElement('restore', selector='restore')
        make_gateway_restore(root)
        alias = Gateways.alias
        namespace = Gateways.namespace
        return xmlutil.MasterTemplate(root, 1, nsmap={alias: namespace})


class CreateDeserializer(wsgi.MetadataXMLDeserializer):
    def default(self, string):
        dom = minidom.parseString(string)
        gateway = self._extract_gateway(dom)
        return {'body': {'gateway': gateway}}

    def _extract_gateway(self, node):
        gateway = {}
        gateway_node = self.find_first_child_named(node, 'gateway')

        attributes = ['container', 'display_name',
                      'display_description', 'volume_id']

        for attr in attributes:
            if gateway_node.getAttribute(attr):
                gateway[attr] = gateway_node.getAttribute(attr)
        return gateway


class RestoreDeserializer(wsgi.MetadataXMLDeserializer):
    def default(self, string):
        dom = minidom.parseString(string)
        restore = self._extract_restore(dom)
        return {'body': {'restore': restore}}

    def _extract_restore(self, node):
        restore = {}
        restore_node = self.find_first_child_named(node, 'restore')
        if restore_node.getAttribute('volume_id'):
            restore['volume_id'] = restore_node.getAttribute('volume_id')
        return restore


class GatewaysController(wsgi.Controller):
    """The Gateways API controller for the OpenStack API."""

    _view_builder_class = gateway_views.ViewBuilder

    def __init__(self):
        self.gateway_api = gatewayAPI.API()
        super(GatewaysController, self).__init__()

    @wsgi.serializers(xml=GatewayTemplate)
    def show(self, req, id):
        """Return data about the given gateway."""
        LOG.debug(_('show called for member %s'), id)
        context = req.environ['cinder.context']

        try:
            gateway = self.gateway_api.get(context, gateway_id=id)
        except exception.GatewayNotFound as error:
            raise exc.HTTPNotFound(explanation=unicode(error))

        return self._view_builder.detail(req, gateway)

    def delete(self, req, id):
        """Delete a gateway."""
        LOG.debug(_('delete called for member %s'), id)
        context = req.environ['cinder.context']

        LOG.audit(_('Delete gateway with id: %s'), id, context=context)

        try:
            self.gateway_api.delete(context, id)
        except exception.GatewayNotFound as error:
            raise exc.HTTPNotFound(explanation=unicode(error))
        except exception.InvalidGateway as error:
            raise exc.HTTPBadRequest(explanation=unicode(error))

        return webob.Response(status_int=202)

    @wsgi.serializers(xml=GatewaysTemplate)
    def index(self, req):
        """Returns a summary list of gateways."""
        return self._get_gateways(req, is_detail=False)

    @wsgi.serializers(xml=GatewaysTemplate)
    def detail(self, req):
        """Returns a detailed list of gateways."""
        return self._get_gateways(req, is_detail=True)

    def _get_gateways(self, req, is_detail):
        """Returns a list of gateways, transformed through view builder."""
        context = req.environ['cinder.context']
        gateways = self.gateway_api.get_all(context)
        limited_list = common.limited(gateways, req)

        if is_detail:
            gateways = self._view_builder.detail_list(req, limited_list)
        else:
            gateways = self._view_builder.summary_list(req, limited_list)
        return gateways

    # TODO(frankm): Add some checks here including
    # - whether requested volume_id exists so we can return some errors
    #   immediately
    # - maybe also do validation of swift container name
    @wsgi.response(202)
    @wsgi.serializers(xml=GatewayTemplate)
    @wsgi.deserializers(xml=CreateDeserializer)
    def create(self, req, body):
        """Create a new gateway."""
        LOG.debug(_('Creating new gateway %s'), body)
        if not self.is_valid_body(body, 'gateway'):
            raise exc.HTTPBadRequest()

        context = req.environ['cinder.context']

        try:
            gateway = body['gateway']
            volume_id = gateway['volume_id']
        except KeyError:
            msg = _("Incorrect request body format")
            raise exc.HTTPBadRequest(explanation=msg)
        container = gateway.get('container', None)
        name = gateway.get('name', None)
        description = gateway.get('description', None)

        LOG.audit(_("Creating gateway of volume %(volume_id)s in container"
                    " %(container)s"),
                  {'volume_id': volume_id, 'container': container},
                  context=context)

        try:
            new_gateway = self.gateway_api.create(context, name, description,
                                                volume_id, container)
        except exception.InvalidVolume as error:
            raise exc.HTTPBadRequest(explanation=unicode(error))
        except exception.VolumeNotFound as error:
            raise exc.HTTPNotFound(explanation=unicode(error))
        except exception.ServiceNotFound as error:
            raise exc.HTTPInternalServerError(explanation=unicode(error))

        retval = self._view_builder.summary(req, dict(new_gateway.iteritems()))
        return retval

    @wsgi.response(202)
    @wsgi.serializers(xml=GatewayRestoreTemplate)
    @wsgi.deserializers(xml=RestoreDeserializer)
    def restore(self, req, id, body):
        """Restore an existing gateway to a volume."""
        LOG.debug(_('Restoring gateway %(gateway_id)s (%(body)s)'),
                  {'gateway_id': id, 'body': body})
        if not self.is_valid_body(body, 'restore'):
            raise exc.HTTPBadRequest()

        context = req.environ['cinder.context']

        try:
            restore = body['restore']
        except KeyError:
            msg = _("Incorrect request body format")
            raise exc.HTTPBadRequest(explanation=msg)
        volume_id = restore.get('volume_id', None)

        LOG.audit(_("Restoring gateway %(gateway_id)s to volume %(volume_id)s"),
                  {'gateway_id': id, 'volume_id': volume_id},
                  context=context)

        try:
            new_restore = self.gateway_api.restore(context,
                                                  gateway_id=id,
                                                  volume_id=volume_id)
        except exception.InvalidInput as error:
            raise exc.HTTPBadRequest(explanation=unicode(error))
        except exception.InvalidVolume as error:
            raise exc.HTTPBadRequest(explanation=unicode(error))
        except exception.InvalidGateway as error:
            raise exc.HTTPBadRequest(explanation=unicode(error))
        except exception.GatewayNotFound as error:
            raise exc.HTTPNotFound(explanation=unicode(error))
        except exception.VolumeNotFound as error:
            raise exc.HTTPNotFound(explanation=unicode(error))
        except exception.VolumeSizeExceedsAvailableQuota as error:
            raise exc.HTTPRequestEntityTooLarge(
                explanation=error.message, headers={'Retry-After': 0})
        except exception.VolumeLimitExceeded as error:
            raise exc.HTTPRequestEntityTooLarge(
                explanation=error.message, headers={'Retry-After': 0})

        retval = self._view_builder.restore_summary(
            req, dict(new_restore.iteritems()))
        return retval


class Gateways(extensions.ExtensionDescriptor):
    """Gateways support."""

    name = 'Gateways'
    alias = 'gateways'
    namespace = 'http://docs.openstack.org/volume/ext/gateways/api/v1'
    updated = '2012-12-12T00:00:00+00:00'

    def get_resources(self):
        resources = []
        res = extensions.ResourceExtension(
            Gateways.alias, GatewaysController(),
            collection_actions={'detail': 'GET'},
            member_actions={'restore': 'POST'})
        resources.append(res)
        return resources
