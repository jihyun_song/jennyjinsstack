
"""
Client side of the volume gateway RPC API.
"""


from oslo.config import cfg

from cinder.openstack.common import log as logging
from cinder.openstack.common import rpc
import cinder.openstack.common.rpc.proxy


CONF = cfg.CONF

LOG = logging.getLogger(__name__)


class GatewayAPI(cinder.openstack.common.rpc.proxy.RpcProxy):
    '''Client side of the volume rpc API.

    API version history:

        1.0 - Initial version.
    '''

    BASE_RPC_API_VERSION = '1.0'

    def __init__(self):
        super(GatewayAPI, self).__init__(
            topic=CONF.gateway_topic,
            default_version=self.BASE_RPC_API_VERSION)

    def create_gateway(self, ctxt, host, gateway_id, volume_id):
        LOG.debug("create_gateway in rpcapi gateway_id %s", gateway_id)
        topic = rpc.queue_get_for(ctxt, self.topic, host)
        LOG.debug("create queue topic=%s", topic)
        self.cast(ctxt,
                  self.make_msg('create_gateway',
                                gateway_id=gateway_id),
                  topic=topic)

    def restore_gateway(self, ctxt, host, gateway_id, volume_id):
        LOG.debug("restore_gateway in rpcapi gateway_id %s", gateway_id)
        topic = rpc.queue_get_for(ctxt, self.topic, host)
        LOG.debug("restore queue topic=%s", topic)
        self.cast(ctxt,
                  self.make_msg('restore_gateway',
                                gateway_id=gateway_id,
                                volume_id=volume_id),
                  topic=topic)

    def delete_gateway(self, ctxt, host, gateway_id):
        LOG.debug("delete_gateway  rpcapi gateway_id %s", gateway_id)
        topic = rpc.queue_get_for(ctxt, self.topic, host)
        self.cast(ctxt,
                  self.make_msg('delete_gateway',
                                gateway_id=gateway_id),
                  topic=topic)
