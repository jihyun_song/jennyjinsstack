
"""
Handles all requests relating to the volume gateways service.
"""


from eventlet import greenthread

from oslo.config import cfg

from cinder.gateway import rpcapi as gateway_rpcapi
from cinder import context
from cinder.db import base
from cinder import exception
from cinder.openstack.common import log as logging
from cinder import utils

import cinder.policy
import cinder.volume

CONF = cfg.CONF
LOG = logging.getLogger(__name__)


def check_policy(context, action):
        target = {
            'project_id': context.project_id,
            'user_id': context.user_id,
        }
        _action = 'gateway:%s' % action
        cinder.policy.enforce(context, _action, target)


class API(base.Base):
    """API for interacting with the volume gateway manager."""

    def __init__(self, db_driver=None):
        self.gateway_rpcapi = gateway_rpcapi.GatewayAPI()
        self.volume_api = cinder.volume.API()
        super(API, self).__init__(db_driver)

    def get(self, context, gateway_id):
        check_policy(context, 'get')
        rv = self.db.gateway_get(context, gateway_id)
        return dict(rv.iteritems())

    def delete(self, context, gateway_id):
        """
        Make the RPC call to delete a volume gateway.
        """
        check_policy(context, 'delete')
        gateway = self.get(context, gateway_id)
        if gateway['status'] not in ['available', 'error']:
            msg = _('GATEWAY status must be available or error')
            raise exception.InvalidBackup(reason=msg)

        self.db.gateway_update(context, gateway_id, {'status': 'deleting'})
        self.gateway_rpcapi.delete_gateway(context,
                                         gateway['host'],
                                         gateway['id'])

    # TODO(moorehef): Add support for search_opts, discarded atm
    def get_all(self, context, search_opts={}):
        check_policy(context, 'get_all')
        if context.is_admin:
            gateways = self.db.gateway_get_all(context)
        else:
            gateways = self.db.gateway_get_all_by_project(context,
                                                        context.project_id)

        return gateways

    def _check_gateway_service(self, volume):
        """
        Check if there is an gateway service available
        """
        topic = CONF.gateway_topic
        ctxt = context.get_admin_context()
        services = self.db.service_get_all_by_topic(ctxt, topic)
        for srv in services:
            if (srv['availability_zone'] == volume['availability_zone'] and
                    srv['host'] == volume['host'] and not srv['disabled'] and
                    utils.service_is_up(srv)):
                return True
        return False

    def create(self, context, name, description, volume_id,
               container, availability_zone=None):
        """
        Make the RPC call to create a volume gateway.
        """
        check_policy(context, 'create')
        volume = self.volume_api.get(context, volume_id)
        if volume['status'] != "available":
            msg = _('Volume to be backed up must be available')
            raise exception.InvalidVolume(reason=msg)
        self.db.volume_update(context, volume_id, {'status': 'updating-gateway'})

        options = {'user_id': context.user_id,
                   'project_id': context.project_id,
                   'display_name': name,
                   'display_description': description,
                   'volume_id': volume_id,
                   'status': 'creating',
                   'container': container,
                   'size': volume['size'],
                   # TODO(DuncanT): This will need de-managling once
                   #                multi-backend lands
                   'host': volume['host'], }

        gateway = self.db.gateway_create(context, options)
        if not self._check_gateway_service(volume):
            raise exception.ServiceNotFound(service_id='cinder-gateway')

        #TODO(DuncanT): In future, when we have a generic local attach,
        #               this can go via the scheduler, which enables
        #               better load ballancing and isolation of services
        self.gateway_rpcapi.create_gateway(context,
                                         gateway['host'],
                                         gateway['id'],
                                         volume_id)

        return gateway

    def restore(self, context, gateway_id, volume_id=None):
        """
        Make the RPC call to restore a volume gateway.
        """
        check_policy(context, 'restore')
        gateway = self.get(context, gateway_id)
        if gateway['status'] != 'available':
            msg = _('Gateway status must be available')
            raise exception.InvalidGateway(reason=msg)

        size = gateway['size']
        if size is None:
            msg = _('Gateway to be restored has invalid size')
            raise exception.InvalidBackup(reason=msg)

        # Create a volume if none specified. If a volume is specified check
        # it is large enough for the gateway
        if volume_id is None:
            name = 'restore_gateway_%s' % gateway_id
            description = 'auto-created_from_restore_from_gateway'

            LOG.audit(_("Creating volume of %(size)s GB for restore of "
                        "gateway %(backup_id)s"),
                      {'size': size, 'gateway_id': gateway_id},
                      context=context)
            volume = self.volume_api.create(context, size, name, description)
            volume_id = volume['id']

            while True:
                volume = self.volume_api.get(context, volume_id)
                if volume['status'] != 'creating':
                    break
                greenthread.sleep(1)
        else:
            volume = self.volume_api.get(context, volume_id)
            volume_size = volume['size']
            if volume_size < size:
                err = (_('volume size %(volume_size)d is too small to restore '
                         'gateway of size %(size)d.') %
                       {'volume_size': volume_size, 'size': size})
                raise exception.InvalidVolume(reason=err)

        if volume['status'] != "available":
            msg = _('Volume to be restored to must be available')
            raise exception.InvalidVolume(reason=msg)

        LOG.debug('Checking gateway size %s against volume size %s',
                  size, volume['size'])
        if size > volume['size']:
            msg = _('Volume to be restored to is smaller '
                    'than the gateway to be restored')
            raise exception.InvalidVolume(reason=msg)

        LOG.audit(_("Overwriting volume %(volume_id)s with restore of "
                    "gateway %(backup_id)s"),
                  {'volume_id': volume_id, 'gateway_id': gateway_id},
                  context=context)

        # Setting the status here rather than setting at start and unrolling
        # for each error condition, it should be a very small window
        self.db.gateway_update(context, gateway_id, {'status': 'restoring'})
        self.db.volume_update(context, volume_id, {'status':
                                                   'restoring-gateway'})
        self.gateway_rpcapi.restore_gateway(context,
                                          gateway['host'],
                                          gateway['id'],
                                          volume_id)

        d = {'gateway_id': gateway_id,
             'volume_id': volume_id, }

        return d
