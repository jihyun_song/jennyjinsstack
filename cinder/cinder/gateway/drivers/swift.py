
"""Implementation of a gateway service that uses Swift as the backend

**Related Flags**

:gateway_swift_url: The URL of the Swift endpoint (default:
                                                        localhost:8080).
:gateway_swift_object_size: The size in bytes of the Swift objects used
                                    for volume gateways (default: 52428800).
:gateway_swift_retry_attempts: The number of retries to make for Swift
                                    operations (default: 10).
:gateway_swift_retry_backoff: The backoff time in seconds between retrying
                                    failed Swift operations (default: 10).
:gateway_compression_algorithm: Compression algorithm to use for volume
                               gateways. Supported options are:
                               None (to disable), zlib and bz2 (default: zlib)
"""

import hashlib
import httplib
import json
import os
import socket
import StringIO

import eventlet
from oslo.config import cfg

from cinder.gateway.driver import GatewayDriver
from cinder import exception
from cinder.openstack.common import log as logging
from cinder.openstack.common import timeutils
from swiftclient import client as swift


LOG = logging.getLogger(__name__)

swiftgateway_service_opts = [
    cfg.StrOpt('gateway_swift_url',
               default='http://localhost:8080/v1/AUTH_',
               help='The URL of the Swift endpoint'),
    cfg.StrOpt('gateway_swift_auth',
               default='per_user',
               help='Swift authentication mechanism'),
    cfg.StrOpt('gateway_swift_user',
               default=None,
               help='Swift user name'),
    cfg.StrOpt('gateway_swift_key',
               default=None,
               help='Swift key for authentication'),
    cfg.StrOpt('gateway_swift_container',
               default='volumegateways',
               help='The default Swift container to use'),
    cfg.IntOpt('gateway_swift_object_size',
               default=52428800,
               help='The size in bytes of Swift gateway objects'),
    cfg.IntOpt('gateway_swift_retry_attempts',
               default=3,
               help='The number of retries to make for Swift operations'),
    cfg.IntOpt('gateway_swift_retry_backoff',
               default=2,
               help='The backoff time in seconds between Swift retries'),
    cfg.StrOpt('gateway_compression_algorithm',
               default='zlib',
               help='Compression algorithm (None to disable)'),
]

CONF = cfg.CONF
CONF.register_opts(swiftgateway_service_opts)


class SwiftGatewayDriver(GatewayDriver):
    """Provides gateway, restore and delete of backup objects within Swift."""

    DRIVER_VERSION = '1.0.0'
    DRIVER_VERSION_MAPPING = {'1.0.0': '_restore_v1'}

    def _get_compressor(self, algorithm):
        try:
            if algorithm.lower() in ('none', 'off', 'no'):
                return None
            elif algorithm.lower() in ('zlib', 'gzip'):
                import zlib as compressor
                return compressor
            elif algorithm.lower() in ('bz2', 'bzip2'):
                import bz2 as compressor
                return compressor
        except ImportError:
            pass

        err = _('unsupported compression algorithm: %s') % algorithm
        raise ValueError(unicode(err))

    def __init__(self, context, db_driver=None):
        self.context = context
        self.swift_url = '%s%s' % (CONF.gateway_swift_url,
                                   self.context.project_id)
        self.az = CONF.storage_availability_zone
        self.data_block_size_bytes = CONF.gateway_swift_object_size
        self.swift_attempts = CONF.gateway_swift_retry_attempts
        self.swift_backoff = CONF.gateway_swift_retry_backoff
        self.compressor = \
            self._get_compressor(CONF.gateway_compression_algorithm)
        LOG.debug('Connect to %s in "%s" mode' % (CONF.gateway_swift_url,
                                                  CONF.gateway_swift_auth))
        if CONF.gateway_swift_auth == 'single_user':
            if CONF.gateway_swift_user is None:
                LOG.error(_("single_user auth mode enabled, "
                            "but %(param)s not set")
                          % {'param': 'gateway_swift_user'})
                raise exception.ParameterNotFound(param='gateway_swift_user')
            self.conn = swift.Connection(authurl=CONF.gateway_swift_url,
                                         user=CONF.gateway_swift_user,
                                         key=CONF.gateway_swift_key,
                                         retries=self.swift_attempts,
                                         starting_backoff=self.swift_backoff)
        else:
            self.conn = swift.Connection(retries=self.swift_attempts,
                                         preauthurl=self.swift_url,
                                         preauthtoken=self.context.auth_token,
                                         starting_backoff=self.swift_backoff)

        super(SwiftGatewayDriver, self).__init__(db_driver)

    def _check_container_exists(self, container):
        LOG.debug(_('_check_container_exists: container: %s') % container)
        try:
            self.conn.head_container(container)
        except swift.ClientException as error:
            if error.http_status == httplib.NOT_FOUND:
                LOG.debug(_('container %s does not exist') % container)
                return False
            else:
                raise
        else:
            LOG.debug(_('container %s exists') % container)
            return True

    def _create_container(self, context, gateway):
        gateway_id = gateway['id']
        container = gateway['container']
        LOG.debug(_('_create_container started, container: %(container)s,'
                    'gateway: %(gateway_id)s') %
                  {'container': container, 'gateway_id': gateway_id})
        if container is None:
            container = CONF.gateway_swift_container
            self.db.gateway_update(context, gateway_id, {'container': container})
        if not self._check_container_exists(container):
            self.conn.put_container(container)
        return container

    def _generate_swift_object_name_prefix(self, gateway):
        az = 'az_%s' % self.az
        gateway_name = '%s_gateway_%s' % (az, gateway['id'])
        volume = 'volume_%s' % (gateway['volume_id'])
        timestamp = timeutils.strtime(fmt="%Y%m%d%H%M%S")
        prefix = volume + '/' + timestamp + '/' + gateway_name
        LOG.debug(_('_generate_swift_object_name_prefix: %s') % prefix)
        return prefix

    def _generate_object_names(self, gateway):
        prefix = gateway['service_metadata']
        swift_objects = self.conn.get_container(gateway['container'],
                                                prefix=prefix,
                                                full_listing=True)[1]
        swift_object_names = [swift_obj['name'] for swift_obj in swift_objects]
        LOG.debug(_('generated object list: %s') % swift_object_names)
        return swift_object_names

    def _metadata_filename(self, gateway):
        swift_object_name = gateway['service_metadata']
        filename = '%s_metadata' % swift_object_name
        return filename

    def _write_metadata(self, gateway, volume_id, container, object_list):
        filename = self._metadata_filename(gateway)
        LOG.debug(_('_write_metadata started, container name: %(container)s,'
                    ' metadata filename: %(filename)s') %
                  {'container': container, 'filename': filename})
        metadata = {}
        metadata['version'] = self.DRIVER_VERSION
        metadata['gateway_id'] = gateway['id']
        metadata['volume_id'] = volume_id
        metadata['gateway_name'] = gateway['display_name']
        metadata['gateway_description'] = gateway['display_description']
        metadata['created_at'] = str(gateway['created_at'])
        metadata['objects'] = object_list
        metadata_json = json.dumps(metadata, sort_keys=True, indent=2)
        reader = StringIO.StringIO(metadata_json)
        etag = self.conn.put_object(container, filename, reader,
                                    content_length=reader.len)
        md5 = hashlib.md5(metadata_json).hexdigest()
        if etag != md5:
            err = _('error writing metadata file to swift, MD5 of metadata'
                    ' file in swift [%(etag)s] is not the same as MD5 of '
                    'metadata file sent to swift [%(md5)s]') % {'etag': etag,
                                                                'md5': md5}
            raise exception.InvalidGateway(reason=err)
        LOG.debug(_('_write_metadata finished'))

    def _read_metadata(self, gateway):
        container = gateway['container']
        filename = self._metadata_filename(gateway)
        LOG.debug(_('_read_metadata started, container name: %(container)s, '
                    'metadata filename: %(filename)s') %
                  {'container': container, 'filename': filename})
        (resp, body) = self.conn.get_object(container, filename)
        metadata = json.loads(body)
        LOG.debug(_('_read_metadata finished (%s)') % metadata)
        return metadata

    def _prepare_gateway(self, gateway):
        """Prepare the gateway process and return the backup metadata"""
        gateway_id = gateway['id']
        volume_id = gateway['volume_id']
        volume = self.db.volume_get(self.context, volume_id)

        if volume['size'] <= 0:
            err = _('volume size %d is invalid.') % volume['size']
            raise exception.InvalidVolume(reason=err)

        try:
            container = self._create_container(self.context, gateway)
        except socket.error as err:
            raise exception.SwiftConnectionFailed(reason=str(err))

        object_prefix = self._generate_swift_object_name_prefix(gateway)
        gateway['service_metadata'] = object_prefix
        self.db.gateway_update(self.context, gateway_id, {'service_metadata':
                                                        object_prefix})
        volume_size_bytes = volume['size'] * 1024 * 1024 * 1024
        availability_zone = self.az
        LOG.debug(_('starting gateway of volume: %(volume_id)s to swift,'
                    ' volume size: %(volume_size_bytes)d, swift object names'
                    ' prefix %(object_prefix)s, availability zone:'
                    ' %(availability_zone)s') %
                  {
                      'volume_id': volume_id,
                      'volume_size_bytes': volume_size_bytes,
                      'object_prefix': object_prefix,
                      'availability_zone': availability_zone,
                  })
        object_meta = {'id': 1, 'list': [], 'prefix': object_prefix}
        return object_meta, container

    def _gateway_chunk(self, gateway, container, data, data_offset, object_meta):
        """Gateway data chunk based on the object metadata and offset"""
        object_prefix = object_meta['prefix']
        object_list = object_meta['list']
        object_id = object_meta['id']
        object_name = '%s-%05d' % (object_prefix, object_id)
        obj = {}
        obj[object_name] = {}
        obj[object_name]['offset'] = data_offset
        obj[object_name]['length'] = len(data)
        LOG.debug(_('reading chunk of data from volume'))
        if self.compressor is not None:
            algorithm = CONF.gateway_compression_algorithm.lower()
            obj[object_name]['compression'] = algorithm
            data_size_bytes = len(data)
            data = self.compressor.compress(data)
            comp_size_bytes = len(data)
            LOG.debug(_('compressed %(data_size_bytes)d bytes of data '
                        'to %(comp_size_bytes)d bytes using '
                        '%(algorithm)s') %
                      {
                          'data_size_bytes': data_size_bytes,
                          'comp_size_bytes': comp_size_bytes,
                          'algorithm': algorithm,
                      })
        else:
            LOG.debug(_('not compressing data'))
            obj[object_name]['compression'] = 'none'

        reader = StringIO.StringIO(data)
        LOG.debug(_('About to put_object'))
        try:
            etag = self.conn.put_object(container, object_name, reader,
                                        content_length=len(data))
        except socket.error as err:
            raise exception.SwiftConnectionFailed(reason=str(err))
        LOG.debug(_('swift MD5 for %(object_name)s: %(etag)s') %
                  {'object_name': object_name, 'etag': etag, })
        md5 = hashlib.md5(data).hexdigest()
        obj[object_name]['md5'] = md5
        LOG.debug(_('gateway MD5 for %(object_name)s: %(md5)s') %
                  {'object_name': object_name, 'md5': md5})
        if etag != md5:
            err = _('error writing object to swift, MD5 of object in '
                    'swift %(etag)s is not the same as MD5 of object sent '
                    'to swift %(md5)s') % {'etag': etag, 'md5': md5}
            raise exception.InvalidGateway(reason=err)
        object_list.append(obj)
        object_id += 1
        object_meta['list'] = object_list
        object_meta['id'] = object_id
        LOG.debug(_('Calling eventlet.sleep(0)'))
        eventlet.sleep(0)

    def _finalize_gateway(self, gateway, container, object_meta):
        """Finalize the gateway by updating its metadata on Swift"""
        object_list = object_meta['list']
        object_id = object_meta['id']
        try:
            self._write_metadata(gateway,
                                 gateway['volume_id'],
                                 container,
                                 object_list)
        except socket.error as err:
            raise exception.SwiftConnectionFailed(reason=str(err))
        self.db.gateway_update(self.context, gateway['id'],
                              {'object_count': object_id})
        LOG.debug(_('gateway %s finished.') % gateway['id'])

    def gateway(self, gateway, volume_file):
        """Gateway the given volume to swift using the given gateway metadata."""
        object_meta, container = self._prepare_gateway(gateway)
        while True:
            data = volume_file.read(self.data_block_size_bytes)
            data_offset = volume_file.tell()
            if data == '':
                break
            self._gateway_chunk(gateway, container, data,
                               data_offset, object_meta)
        self._finalize_gateway(gateway, container, object_meta)

    def _restore_v1(self, gateway, volume_id, metadata, volume_file):
        """Restore a v1 swift volume gateway from swift."""
        gateway_id = gateway['id']
        LOG.debug(_('v1 swift volume gateway restore of %s started'), gateway_id)
        container = gateway['container']
        metadata_objects = metadata['objects']
        metadata_object_names = sum((obj.keys() for obj in metadata_objects),
                                    [])
        LOG.debug(_('metadata_object_names = %s') % metadata_object_names)
        prune_list = [self._metadata_filename(gateway)]
        swift_object_names = [swift_object_name for swift_object_name in
                              self._generate_object_names(gateway)
                              if swift_object_name not in prune_list]
        if sorted(swift_object_names) != sorted(metadata_object_names):
            err = _('restore_gateway aborted, actual swift object list in '
                    'swift does not match object list stored in metadata')
            raise exception.InvalidGateway(reason=err)

        for metadata_object in metadata_objects:
            object_name = metadata_object.keys()[0]
            LOG.debug(_('restoring object from swift. gateway: %(gateway_id)s, '
                        'container: %(container)s, swift object name: '
                        '%(object_name)s, volume: %(volume_id)s') %
                      {
                          'gateway_id': gateway_id,
                          'container': container,
                          'object_name': object_name,
                          'volume_id': volume_id,
                      })
            try:
                (resp, body) = self.conn.get_object(container, object_name)
            except socket.error as err:
                raise exception.SwiftConnectionFailed(reason=str(err))
            compression_algorithm = metadata_object[object_name]['compression']
            decompressor = self._get_compressor(compression_algorithm)
            if decompressor is not None:
                LOG.debug(_('decompressing data using %s algorithm') %
                          compression_algorithm)
                decompressed = decompressor.decompress(body)
                volume_file.write(decompressed)
            else:
                volume_file.write(body)

            # force flush every write to avoid long blocking write on close
            volume_file.flush()

            # Be tolerant to IO implementations that do not support fileno()
            try:
                fileno = volume_file.fileno()
            except IOError:
                LOG.info("volume_file does not support fileno() so skipping "
                         "fsync()")
            else:
                os.fsync(fileno)

            # Restoring a gateway to a volume can take some time. Yield so other
            # threads can run, allowing for among other things the service
            # status to be updated
            eventlet.sleep(0)
        LOG.debug(_('v1 swift volume gateway restore of %s finished'),
                  gateway_id)

    def restore(self, gateway, volume_id, volume_file):
        """Restore the given volume gateway from swift."""
        gateway_id = gateway['id']
        container = gateway['container']
        object_prefix = gateway['service_metadata']
        LOG.debug(_('starting restore of gateway %(object_prefix)s from swift'
                    ' container: %(container)s, to volume %(volume_id)s, '
                    'gateway: %(gateway_id)s') %
                  {
                      'object_prefix': object_prefix,
                      'container': container,
                      'volume_id': volume_id,
                      'gateway_id': gateway_id,
                  })
        try:
            metadata = self._read_metadata(gateway)
        except socket.error as err:
            raise exception.SwiftConnectionFailed(reason=str(err))
        metadata_version = metadata['version']
        LOG.debug(_('Restoring swift gateway version %s'), metadata_version)
        try:
            restore_func = getattr(self, self.DRIVER_VERSION_MAPPING.get(
                metadata_version))
        except TypeError:
            err = (_('No support to restore swift gateway version %s')
                   % metadata_version)
            raise exception.InvalidGateway(reason=err)
        restore_func(gateway, volume_id, metadata, volume_file)
        LOG.debug(_('restore %(gateway_id)s to %(volume_id)s finished.') %
                  {'gateway_id': gateway_id, 'volume_id': volume_id})

    def delete(self, gateway):
        """Delete the given gateway from swift."""
        container = gateway['container']
        LOG.debug('delete started, gateway: %s, container: %s, prefix: %s',
                  gateway['id'], container, gateway['service_metadata'])

        if container is not None:
            swift_object_names = []
            try:
                swift_object_names = self._generate_object_names(gateway)
            except Exception:
                LOG.warn(_('swift error while listing objects, continuing'
                           ' with delete'))

            for swift_object_name in swift_object_names:
                try:
                    self.conn.delete_object(container, swift_object_name)
                except socket.error as err:
                    raise exception.SwiftConnectionFailed(reason=str(err))
                except Exception:
                    LOG.warn(_('swift error while deleting object %s, '
                               'continuing with delete') % swift_object_name)
                else:
                    LOG.debug(_('deleted swift object: %(swift_object_name)s'
                                ' in container: %(container)s') %
                              {
                                  'swift_object_name': swift_object_name,
                                  'container': container
                              })
                # Deleting a gateway's objects from swift can take some time.
                # Yield so other threads can run
                eventlet.sleep(0)

        LOG.debug(_('delete %s finished') % gateway['id'])


def get_gateway_driver(context):
    return SwiftGatewayDriver(context)
