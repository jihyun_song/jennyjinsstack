
"""
Gateway manager manages volume gateways.

Volume Gateways are full copies of persistent volumes stored in a gateway
store e.g. an object store or any other gateway store if and when support is
added. They are usable without the original object being available. A
volume gateway can be restored to the original volume it was created from or
any other available volume with a minimum size of the original volume.
Volume gateways can be created, restored, deleted and listed.

**Related Flags**

:gateway_topic:  What :mod:`rpc` topic to listen to (default:
                        `cinder-gateway`).
:gateway_manager:  The module name of a class derived from
                          :class:`manager.Manager` (default:
                          :class:`cinder.gateway.manager.Manager`).

"""

from oslo.config import cfg

from cinder import context
from cinder import exception
from cinder import manager
from cinder.openstack.common import excutils
from cinder.openstack.common import importutils
from cinder.openstack.common import log as logging


LOG = logging.getLogger(__name__)

gateway_manager_opts = [
    cfg.StrOpt('gateway_driver',
               default='cinder.gateway.drivers.swift',
               help='Driver to use for gateways.',
               deprecated_name='gateway_service'),
]

# This map doesn't need to be extended in the future since it's only
# for old gateway services
mapper = {'cinder.gateway.services.swift': 'cinder.gateway.drivers.swift',
          'cinder.gateway.services.ceph': 'cinder.gateway.drivers.ceph'}

CONF = cfg.CONF
CONF.register_opts(gateway_manager_opts)


class GatewayManager(manager.SchedulerDependentManager):
    """Manages gateway of block storage devices."""

    RPC_API_VERSION = '1.0'

    def __init__(self, service_name=None, *args, **kwargs):
        self.service = importutils.import_module(self.driver_name)
        self.az = CONF.storage_availability_zone
        self.volume_manager = importutils.import_object(
            CONF.volume_manager)
        self.driver = self.volume_manager.driver
        super(GatewayManager, self).__init__(service_name='gateway',
                                            *args, **kwargs)
        self.driver.db = self.db

    @property
    def driver_name(self):
        """This function maps old gateway services to gateway drivers."""

        return self._map_service_to_driver(CONF.gateway_driver)

    def _map_service_to_driver(self, service):
        """Maps services to drivers."""

        if service in mapper:
            return mapper[service]
        return service

    def init_host(self):
        """Do any initialization that needs to be run if this is a
           standalone service.
        """

        ctxt = context.get_admin_context()
        self.driver.do_setup(ctxt)
        self.driver.check_for_setup_error()

        LOG.info(_("Cleaning up incomplete gateway operations"))
        volumes = self.db.volume_get_all_by_host(ctxt, self.host)
        for volume in volumes:
            if volume['status'] == 'cinder-gateway':
                LOG.info(_('Resetting volume %s to available '
                           '(was backing-up)') % volume['id'])
                self.volume_manager.detach_volume(ctxt, volume['id'])
            if volume['status'] == 'restoring-gateway':
                LOG.info(_('Resetting volume %s to error_restoring '
                           '(was restoring-gateway)') % volume['id'])
                self.volume_manager.detach_volume(ctxt, volume['id'])
                self.db.volume_update(ctxt, volume['id'],
                                      {'status': 'error_restoring'})

        # TODO(smulcahy) implement full resume of gateway and restore
        # operations on restart (rather than simply resetting)
        gateways = self.db.gateway_get_all_by_host(ctxt, self.host)
        for gateway in gateways:
            if gateway['status'] == 'creating':
                LOG.info(_('Resetting gateway %s to error '
                           '(was creating)') % gateway['id'])
                err = 'incomplete gateway reset on manager restart'
                self.db.gateway_update(ctxt, gateway['id'], {'status': 'error',
                                                           'fail_reason': err})
            if gateway['status'] == 'restoring':
                LOG.info(_('Resetting gateway %s to available '
                           '(was restoring)') % gateway['id'])
                self.db.gateway_update(ctxt, gateway['id'],
                                      {'status': 'available'})
            if gateway['status'] == 'deleting':
                LOG.info(_('Resuming delete on gateway: %s') % gateway['id'])
                self.delete_gateway(ctxt, gateway['id'])

    def create_gateway(self, context, gateway_id):
        """
        Create volume gateways using configured backup service.
        """
        gateway = self.db.gateway_get(context, gateway_id)
        volume_id = gateway['volume_id']
        volume = self.db.volume_get(context, volume_id)
        LOG.info(_('create_gateway started, gateway: %(gateway_id)s for '
                   'volume: %(volume_id)s') %
                 {'gateway_id': gateway_id, 'volume_id': volume_id})
        self.db.gateway_update(context, gateway_id, {'host': self.host,
                                                   'service':
                                                   self.driver_name})

        expected_status = 'backing-up'
        actual_status = volume['status']
        if actual_status != expected_status:
            err = _('create_gateway aborted, expected volume status '
                    '%(expected_status)s but got %(actual_status)s') % {
                        'expected_status': expected_status,
                        'actual_status': actual_status,
                    }
            self.db.gateway_update(context, gateway_id, {'status': 'error',
                                                       'fail_reason': err})
            raise exception.InvalidVolume(reason=err)

        expected_status = 'creating'
        actual_status = gateway['status']
        if actual_status != expected_status:
            err = _('create_gateway aborted, expected backup status '
                    '%(expected_status)s but got %(actual_status)s') % {
                        'expected_status': expected_status,
                        'actual_status': actual_status,
                    }
            self.db.volume_update(context, volume_id, {'status': 'available'})
            self.db.gateway_update(context, gateway_id, {'status': 'error',
                                                       'fail_reason': err})
            raise exception.InvalidGateway(reason=err)

        try:
            gateway_service = self.service.get_gateway_driver(context)
            self.driver.gateway_volume(context, gateway, gateway_service)
        except Exception as err:
            with excutils.save_and_reraise_exception():
                self.db.volume_update(context, volume_id,
                                      {'status': 'available'})
                self.db.gateway_update(context, gateway_id,
                                      {'status': 'error',
                                       'fail_reason': unicode(err)})

        self.db.volume_update(context, volume_id, {'status': 'available'})
        self.db.gateway_update(context, gateway_id, {'status': 'available',
                                                   'size': volume['size'],
                                                   'availability_zone':
                                                   self.az})
        LOG.info(_('create_gateway finished. backup: %s'), gateway_id)

    def restore_gateway(self, context, gateway_id, volume_id):
        """
        Restore volume gateways from configured backup service.
        """
        LOG.info(_('restore_gateway started, restoring backup: %(gateway_id)s'
                   ' to volume: %(volume_id)s') %
                 {'gateway_id': gateway_id, 'volume_id': volume_id})
        gateway = self.db.gateway_get(context, gateway_id)
        volume = self.db.volume_get(context, volume_id)
        self.db.gateway_update(context, gateway_id, {'host': self.host})

        expected_status = 'restoring-gateway'
        actual_status = volume['status']
        if actual_status != expected_status:
            err = _('restore_gateway aborted, expected volume status '
                    '%(expected_status)s but got %(actual_status)s') % {
                        'expected_status': expected_status,
                        'actual_status': actual_status
                    }
            self.db.gateway_update(context, gateway_id, {'status': 'available'})
            raise exception.InvalidVolume(reason=err)

        expected_status = 'restoring'
        actual_status = gateway['status']
        if actual_status != expected_status:
            err = _('restore_gateway aborted, expected backup status '
                    '%(expected_status)s but got %(actual_status)s') % {
                        'expected_status': expected_status,
                        'actual_status': actual_status
                    }
            self.db.gateway_update(context, gateway_id, {'status': 'error',
                                                       'fail_reason': err})
            self.db.volume_update(context, volume_id, {'status': 'error'})
            raise exception.InvalidGateway(reason=err)

        if volume['size'] > gateway['size']:
            LOG.warn('volume: %s, size: %d is larger than gateway: %s, '
                     'size: %d, continuing with restore',
                     volume['id'], volume['size'],
                     gateway['id'], gateway['size'])

        gateway_service = self._map_service_to_driver(gateway['service'])
        configured_service = self.driver_name
        if gateway_service != configured_service:
            err = _('restore_gateway aborted, the backup service currently'
                    ' configured [%(configured_service)s] is not the'
                    ' gateway service that was used to create this'
                    ' gateway [%(gateway_service)s]') % {
                        'configured_service': configured_service,
                        'gateway_service': gateaway_service,
                    }
            self.db.gateway_update(context, gateway_id, {'status': 'available'})
            self.db.volume_update(context, volume_id, {'status': 'error'})
            raise exception.InvalidGateway(reason=err)

        try:
            gateway_service = self.service.get_gateway_driver(context)
            self.driver.restore_gateway(context, gateway, volume,
                                       gateway_service)
        except Exception as err:
            with excutils.save_and_reraise_exception():
                self.db.volume_update(context, volume_id,
                                      {'status': 'error_restoring'})
                self.db.gateway_update(context, gateway_id,
                                      {'status': 'available'})

        self.db.volume_update(context, volume_id, {'status': 'available'})
        self.db.gateway_update(context, gateway_id, {'status': 'available'})
        LOG.info(_('restore_gateway finished, backup: %(gateway_id)s restored'
                   ' to volume: %(volume_id)s') %
                 {'gateway_id': gateway_id, 'volume_id': volume_id})

    def delete_gateway(self, context, gateway_id):
        """
        Delete volume gateway from configured backup service.
        """
        gateway = self.db.gateway_get(context, gateway_id)
        LOG.info(_('delete_gateway started, backup: %s'), gateway_id)
        self.db.gateway_update(context, gateway_id, {'host': self.host})

        expected_status = 'deleting'
        actual_status = gateway['status']
        if actual_status != expected_status:
            err = _('delete_gateway aborted, expected backup status '
                    '%(expected_status)s but got %(actual_status)s') % {
                        'expected_status': expected_status,
                        'actual_status': actual_status,
                    }
            self.db.gateway_update(context, gateway_id, {'status': 'error',
                                                       'fail_reason': err})
            raise exception.InvalidGateway(reason=err)

        gateway_service = self._map_service_to_driver(gateway['service'])
        if gateway_service is not None:
            configured_service = self.driver_name
            if gateway_service != configured_service:
                err = _('delete_gateway aborted, the backup service currently'
                        ' configured [%(configured_service)s] is not the'
                        ' gateway service that was used to create this'
                        ' gateway [%(backup_service)s]') % {
                            'configured_service': configured_service,
                            'gateway_service': gateway_service,
                        }
                self.db.gateway_update(context, gateway_id,
                                      {'status': 'error'})
                raise exception.InvalidGateway(reason=err)

            try:
                gateway_service = self.service.get_gateway_driver(context)
                gateway_service.delete(gateway)
            except Exception as err:
                with excutils.save_and_reraise_exception():
                    self.db.gateway_update(context, gateway_id,
                                          {'status': 'error',
                                           'fail_reason':
                                           unicode(err)})

        context = context.elevated()
        self.db.gateway_destroy(context, gateway_id)
        LOG.info(_('delete_gateway finished, gateway %s deleted'), gateway_id)
